package com.example.customerservicekafka.mappers;


import com.example.customerservicekafka.dto.CustomerRequestDTO;
import com.example.customerservicekafka.dto.CustomerResponseDTO;
import com.example.customerservicekafka.entities.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    @Mapping(source = "email", target = "emailDto")
    CustomerResponseDTO customerToCustomerResponseDTO(Customer customer);

    Customer customerRequestDtoToCustomer(CustomerRequestDTO customerRequestDTO);


}
