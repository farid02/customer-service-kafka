package com.example.customerservicekafka.service;

import com.example.customerservicekafka.dto.CustomerRequestDTO;
import com.example.customerservicekafka.dto.CustomerResponseDTO;
import com.example.customerservicekafka.entities.Customer;
import com.example.customerservicekafka.mappers.CustomerMapper;
import com.example.customerservicekafka.repositories.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;
    private CustomerMapper customerMapper;


    public CustomerServiceImpl(CustomerRepository customerRepository, CustomerMapper customerMapper) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
    }

    @Override
    public CustomerResponseDTO save(CustomerRequestDTO customerRequestDTO) {
        Customer customer = customerMapper.customerRequestDtoToCustomer(customerRequestDTO);
        Customer saveCustomer = customerRepository.save(customer);
        CustomerResponseDTO customerResponseDTO = customerMapper.customerToCustomerResponseDTO(saveCustomer);
        return customerResponseDTO;
    }


    @Override
    public CustomerResponseDTO getCustomer(String id) {
        Customer customer = customerRepository.getById(id);
        return customerMapper.customerToCustomerResponseDTO(customer);

    }

    @Override
    public CustomerResponseDTO update(CustomerRequestDTO customerRequestDTO) {
        Customer customer = customerMapper.customerRequestDtoToCustomer(customerRequestDTO);
        Customer updatedCustomer = customerRepository.save(customer);
        return customerMapper.customerToCustomerResponseDTO(updatedCustomer);
    }

    @Override
    public List<CustomerResponseDTO> listCustomers() {
        List<Customer> customers = customerRepository.findAll();
        return customers.stream().
                map(customer -> customerMapper.customerToCustomerResponseDTO(customer)).
                collect(Collectors.toList());
    }
}
