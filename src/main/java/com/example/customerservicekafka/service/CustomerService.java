package com.example.customerservicekafka.service;

import com.example.customerservicekafka.dto.CustomerRequestDTO;
import com.example.customerservicekafka.dto.CustomerResponseDTO;

import java.util.List;

public interface CustomerService {

    CustomerResponseDTO save(CustomerRequestDTO casRequestDTO);

    CustomerResponseDTO getCustomer(String id);

    CustomerResponseDTO update(CustomerRequestDTO casRequestDTO);

    List<CustomerResponseDTO> listCustomers();


}
